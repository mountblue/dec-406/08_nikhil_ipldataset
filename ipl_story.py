import csv
from pprint import pprint
from collections import OrderedDict
import matplotlib.pyplot as plt


def get_match_id(year):

    match_ids = []
    matches_reader = csv.DictReader(open('static/matches.csv'))

    for match in matches_reader:
        if match["season"] == str(year):
            match_ids.append(match["id"])
    return match_ids


def get_top_scorer(match_ids):
    """
    batsmen_dict = {
        name: [runs, num_dismissal]
    }
    """
    batsmen_dict = {}
    deliveries_reader = csv.DictReader(open('static/deliveries.csv'))

    for deliveries in deliveries_reader:
        batsman = deliveries["batsman"]
        if deliveries["match_id"] in match_ids:
            if batsman in batsmen_dict:
                batsmen_dict[batsman] += int(deliveries["batsman_runs"])
            else:
                batsmen_dict[batsman] = int(deliveries["batsman_runs"])

    batsman_scores = OrderedDict(sorted(batsmen_dict.items(), key=lambda kv: kv[1])[:10:-1])
    top_ten = list(batsman_scores.keys())[:10]
    return [top_ten, batsman_scores]


def plot_graph(top_performer, year, choice):
    names = top_performer[0]

    if choice == "bat":
        plt.title("Top Ten Run Score of {0}".format(year))
        plt.xlabel("Batsmen")
        plt.ylabel("Score")
    else:
        plt.title("Top Ten Wicket take of {0}".format(year))
        plt.xlabel("Bowler")
        plt.ylabel("Wicket")

    plt.figure(1, figsize=(10, 6))

    for name in names:
        print("{0} --> {1}".format(name, top_performer[1][name]))
        plt.bar(name, top_performer[1][name], width=0.6)

    plt.show()


def get_top_bowler(match_ids):

    bowler_dict = {}
    deliveries_reader = csv.DictReader(open('static/deliveries.csv'))

    for deliveries in deliveries_reader:
        bowler = deliveries["bowler"]
        if deliveries["match_id"] in match_ids:
            if deliveries["player_dismissed"]:
                if bowler in bowler_dict:
                    bowler_dict[bowler] += 1
                else:
                    bowler_dict[bowler] = 1

    bowlers_wicket = OrderedDict(sorted(bowler_dict.items(), key=lambda kv: kv[1])[:10:-1])
    top_ten = list(bowlers_wicket.keys())[:10]
    return [top_ten, bowlers_wicket]


def main():

    choice = input("Choose 1 to check top 10 run getter or choose 2 to check top 10 wicket taker | 0 is exit")

    while choice:
        if choice == "1":
            year = input("Enter the year of which top ten run scorer is to be seen, 0 to exit \n")
            if int(year) <= 2017 and int(year) >= 2008:
                match_ids = get_match_id(year)
                top_performer = get_top_scorer(match_ids)
                plot_graph(top_performer, year, choice)

            elif int(year) == 0:
                break

            else:
                print("Invalid year, Data available between 2008 and 2017")

        elif choice == "2":
            year = input("Enter the year of which top ten wicket taker is to be seen, 0 to exit\n")
            if int(year) <= 2017 and int(year) >= 2008:
                match_ids = get_match_id(year)
                top_performer = get_top_bowler(match_ids)
                plot_graph(top_performer, year, choice)

            elif int(year) == 0:
                break

            else:
                print("Invalid year, Data available between 2008 and 2017 \n")

        elif choice == "0":
            break

        choice = input("Choose betwenn 0, 1, 2")


main()
