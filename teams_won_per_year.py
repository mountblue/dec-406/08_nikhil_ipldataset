import csv
import matplotlib.pyplot as plt
from collections import OrderedDict
from pprint import pprint


def get_winners_per_season():
    teams = set()
    winners_per_season = {}

    with open('static/matches.csv') as file:

        matches_reader = csv.DictReader(file)

        for match in matches_reader:
            teams.add(match["team1"])
            year = match["season"]
            winner = match["winner"]
            if winner:
                if year not in winners_per_season:
                    winners_per_season[year] = {}

                if winner in winners_per_season[year]:
                    winners_per_season[year][winner] += 1
                else:
                    winners_per_season[year][winner] = 1

    pprint(winners_per_season)

    # adding all teams in all year
    for year in winners_per_season:
        teams_in_season = winners_per_season[year].keys()
        for team in teams:
            if team not in teams_in_season:
                winners_per_season[year][team] = "0"

    winners_per_season = OrderedDict(sorted(winners_per_season.items(), key=lambda kv: kv[0]))

    # sort all data
    for year in winners_per_season:
        winners_per_season[year] = OrderedDict(sorted(winners_per_season[year].items(), key=lambda kv: kv[0]))

    return winners_per_season


def plot_graph(winners_per_season):
    colors = {'Chennai Super Kings': 'y', 'Deccan Chargers': 'Teal', 'Delhi Daredevils': 'Maroon',
            'Gujarat Lions': 'Aqua', 'Kings XI Punjab': 'r', 'Kochi Tuskers Kerala': 'c',
            'Kolkata Knight Riders': 'Navy', 'Mumbai Indians': 'b', 'Pune Warriors': 'g',
            'Rajasthan Royals': 'Silver', 'Rising Pune Supergiant': 'Purple',
            'Royal Challengers Bangalore': 'Lime', 'Sunrisers Hyderabad': 'm'}
    key = sorted(colors.keys())

    for year in winners_per_season:
        bottom = 0
        for team, matches_won in winners_per_season[year].items():
            plt.bar(year, int(matches_won), bottom=bottom, color=colors[team])
            bottom += int(matches_won)

    plt.xlabel("YEARS")
    plt.ylabel("MATCHES WON")
    plt.legend(key, fontsize="xx-small")
    plt.show()


def main():
    winners_per_season_per_year = get_winners_per_season()
    plot_graph(winners_per_season_per_year)


main()
