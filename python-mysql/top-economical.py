import mysql.connector

try:
    con = mysql.connector.connect(
        host="localhost",
        user="nike",
        password="root",
        database="test"
    )

    cursor = con.cursor()

    query = """
            SELECT bowler,
            ((sum(total_runs) - sum(legbye_runs) - sum(bye_runs)) / ((count(total_runs)-
            (sum(case when wide_runs=0 then 0 else 1 end) +
            sum(case when noball_runs=0 then 0 else 1 end))) / 6)) as economy
            FROM deliveries
            INNER JOIN matches
            ON deliveries.match_id = matches.id
            where season = 2015
            GROUP BY bowler
            ORDER BY economy
            LIMIT 10
    """

    cursor.execute(query)

    for eco_per_bowler in cursor:
        print("{0} \t -->  {1}".format(eco_per_bowler[0], eco_per_bowler[1]))

except mysql.connector.Error as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if con:
        con.close()
