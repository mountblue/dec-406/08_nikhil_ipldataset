import mysql.connector

try:
    con = mysql.connector.connect(
        host="localhost",
        user="nike",
        password="root",
        database="test"
    )

    cursor = con.cursor()

    choice = input("Choose 1 to check top 10 run getter or choose 2 to check top 10 wicket taker | 0 is exit")
    year = input(" Mention the year between 2008 and 2017 ")

    if choice == "1":
        query = """
                SELECT batsman, SUM(batsman_runs) as runs_scored
                FROM deliveries
                INNER JOIN matches
                ON match_id = id
                WHERE season = %s
                GROUP BY batsman
                ORDER BY runs_scored DESC
                LIMIT 10
        """

    elif choice == "2":
        query = """
                SELECT bowler, SUM(case when player_dismissed = '' then 0 else 1 end) as wickets
                FROM deliveries
                INNER JOIN matches
                ON match_id = id
                WHERE season = %s
                GROUP BY bowler
                ORDER BY wickets DESC
                LIMIT 10
        """

    if int(choice) in range(1, 3) and int(year) in range(2008, 2018):
        cursor.execute(query, (year,))

        for performence in cursor:
            print("{0} --> {1}".format(performence[0], performence[1]))

    else:
        print("Invalid choice/year")

except mysql.connector.Error as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if con:
        con.close()
