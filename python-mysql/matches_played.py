import mysql.connector

try:
    con = mysql.connector.connect(
        host="localhost",
        user="nike",
        password="root",
        database="test"
    )

    cursor = con.cursor()

    query = "SELECT season, count(season) as matches_won FROM matches GROUP BY season"

    cursor.execute(query)

    for matches_won in cursor:
        print(matches_won)

except mysql.connector.Error as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if con:
        con.close()
