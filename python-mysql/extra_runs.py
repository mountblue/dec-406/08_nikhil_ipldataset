import mysql.connector

try:
    con = mysql.connector.connect(
        host="localhost",
        user="nike",
        password="root",
        database="test"
    )

    cursor = con.cursor()

    query = """
            SELECT bowling_team, sum(extra_runs) as runs_given
            FROM deliveries
            INNER JOIN matches
            ON deliveries.match_id = matches.id
            WHERE matches.season = 2016
            GROUP BY bowling_team;
    """

    cursor.execute(query)

    for extra_runs_per_team in cursor:
        print("{0} --> {1}".format(extra_runs_per_team[0], extra_runs_per_team[1]))

except mysql.connector.Error as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if con:
        con.close()
