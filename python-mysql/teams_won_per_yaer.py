import mysql.connector

try:
    con = mysql.connector.connect(
        host="localhost",
        user="nike",
        password="root",
        database="test"
    )

    cursor = con.cursor()

    query = """SELECT season, winner, count(winner) as matches_won
            FROM matches
            GROUP BY season, winner
            ORDER BY season
        """

    cursor.execute(query)

    year_check = "0"
    for matches_won in cursor:
        if matches_won[1] != "":
            if year_check != matches_won[0]:
                year_check = matches_won[0]
                print("\n")
                print("IN THE YEAR {}".format(matches_won[0]))
                print("=========================")
            print("\n")
            print("{0} --> {1}".format(matches_won[1], matches_won[2]))

except mysql.connector.Error as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if con:
        con.close()
